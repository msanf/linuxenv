# Create sane programming environment
# -o errexit    or -e : Exit on command failure
# -o nounset    or -u : Exit on unset variable
# -o pipefail   or -p : Exit on failure of piped commands
# -o noclobber  or -C : Prevent overwriting of files
# -o xtrace     or -x : Verbose expanded commands
set -eup

# Default parameters
VERBOSE=1
HELP=n
PATH=./

########################################
# Parse Arguments
# Use enhanced getopt
########################################

# Test for enhanced getopt
! getopt --test > /dev/null
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
  echo 'ERROR: This environment does not support enhanced getopt()'
  exit 1
fi

# Check formatting of arguments
OPTIONS=v:h
LONGOPTS=verbose:,help
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
  # Exit if argument parsing fails.
  exit 2
fi
eval set -- "$PARSED"

# Evaluate arguments
while true; do
  case "$1" in
    -v|--verbose)
      VERBOSE=y
      shift 2
      ;;
    -h|--help)
      HELP=y
      shift
      ;;
    --)
      shift
      break
      ;;
    *)
      echo "ERROR: Unable to parse command line arguments."
      exit 3
      ;;
  esac
done

# handle non-option arguments
if [[ $# -ne 1 ]]; then
  echo "MISSING ARGUMENT"
else
  PATH=$1
fi

# Print help
if [[ "$HELP" == "y" ]]; then
cat << EndOfMessage
Usage: "$0" [OPTION]... PATH...
       "$0" [OPTION]...
Options:
  -v, --verbose [int]    prints additional information during execution
  -h, --help             print this message
 
EndOfMessage
fi
