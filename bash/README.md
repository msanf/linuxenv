#bash

Some bash user scripts.

## Quick & Dirty File Hashing
```
# Determine file list while excluding .chk file and .git directories.
find -type f ! -name '*.chk' ! -path '*.git*'
# Create a hash list
find -type f ! -name '*.chk' ! -path '*.git*' -exec md5sum "{}" + > checklist.chk
# Verify list
md5sum -c checklist.chk
```


