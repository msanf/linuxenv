#Check for current settings file
$file_exists = Test-Path $Profile
if ( ! $file_exists) {
	Write-Output "Settings file does not exist. Crating file at '$Profile'"
	New-Item –Path $Profile –Type File –Force
}
#Open settings file
$file_exists = Test-Path $Profile
if ( $file_exists) {
	start $Profile
} else {
	Write-Output "Failed to open settings file."
}
